import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { FileUploadModule } from 'primeng/fileupload';
import { HttpClientModule } from '@angular/common/http';
import { InputTextModule } from 'primeng/inputtext';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { RadioButtonModule } from 'primeng/radiobutton';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastModule } from 'primeng/toast';
import { ConfigureDataComponent } from './configure-data/configure-data.component';

@NgModule({
  declarations: [FileUploadComponent, ConfigureDataComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    InputTextModule,
    FormsModule,
    FileUploadModule,
    ButtonModule,
    ToastModule,
    BrowserAnimationsModule,
    RadioButtonModule,
  ],
})
export class HomeModule {}
