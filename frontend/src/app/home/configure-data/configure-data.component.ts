import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil } from 'rxjs';
import { FileUploadService } from 'src/app/api/file-upload.service';

@Component({
  selector: 'app-configure-data',
  templateUrl: './configure-data.component.html',
  styleUrls: ['./configure-data.component.scss'],
})
export class ConfigureDataComponent implements OnInit, OnDestroy {
  jsonEdiFile;
  jsonDestroy$ = new Subject();

  constructor(private readonly fileUploadService: FileUploadService) {}

  ngOnDestroy(): void {
    this.jsonDestroy$.next('');
    this.jsonDestroy$.complete();
  }

  ngOnInit(): void {
    this.fileUploadService.jsonData$
      .pipe(takeUntil(this.jsonDestroy$))
      .subscribe((data) => {
        this.jsonEdiFile = JSON.parse(JSON.stringify(data));
      });
  }
}
