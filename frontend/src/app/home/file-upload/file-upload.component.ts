import { Component, OnInit } from '@angular/core';
import { MessageService } from 'primeng/api';
import { FileUploadService } from 'src/app/api/file-upload.service';

@Component({
  selector: 'app-file-upload',
  templateUrl: './file-upload.component.html',
  styleUrls: ['./file-upload.component.scss'],
})
export class FileUploadComponent implements OnInit {
  uplodedFileName: any;
  showOptionsFlag = false;
  UploadedFileData: any;
  constructor(
    private messageService: MessageService,
    private readonly fileUploadService: FileUploadService
  ) {}

  shipmentID: string;
  orgID: string;
  selectedFileType: string;
  fileData: FormData;
  ediJSONFile;

  ngOnInit(): void {}

  onUpload(event, id) {
    const formData = new FormData();
    this.uplodedFileName = event[0];
    formData.append('ediFile', event[0]);
    this.fileData = formData;
  }
  onUploadFile() {
    this.fileUploadService
      .postEDIFile(this.fileData, this.selectedFileType)
      .subscribe((data: any) => {
        if (data) {
          this.messageService.add({
            severity: 'success',
            summary: 'Success',
            detail: 'FIle has been successfully uploaded',
          });
          this.UploadedFileData = data;
          this.showOptionsFlag = true;
          if (this.selectedFileType === 'JSON') {
            this.fileUploadService.syncJSONData(this.UploadedFileData);
          }
        }
      });
  }

  onDownloadFile() {
    const filename = this.uplodedFileName?.name.split('.');
    this.ediJSONFile = JSON.parse(JSON.stringify(this.UploadedFileData));
    if (this.selectedFileType === 'JSON') {
      const jsonString = JSON.stringify(this.UploadedFileData, null, 2);
      const blob = new Blob([jsonString], { type: 'application/json' });
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = filename[0];
      link.click();
      URL.revokeObjectURL(link.href);
    } else {
      const decoder = new TextDecoder();
      const xmlString = decoder.decode(this.UploadedFileData);
      const parser = new DOMParser();
      const xmlDoc = parser.parseFromString(xmlString, 'application/xml');
      const xmlStringResult = new XMLSerializer().serializeToString(xmlDoc);
      const blob = new Blob([xmlStringResult], {
        type: 'application/xml',
      });
      const link = document.createElement('a');
      link.href = URL.createObjectURL(blob);
      link.download = filename[0];
      link.click();
    }
  }
}
