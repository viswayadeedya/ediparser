import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FileUploadService {
  private readonly jsonDataSubject: BehaviorSubject<any> = new BehaviorSubject(
    {}
  );
  jsonData$ = this.jsonDataSubject.asObservable();
  constructor(private readonly httpClient: HttpClient) {}

  postEDIFile(file, fileType) {
    const url = `http://localhost:9090/upload?fileType=${fileType}`;
    if (fileType === 'JSON') {
      return this.httpClient.post(url, file);
    } else {
      return this.httpClient.post(url, file, { responseType: 'arraybuffer' });
    }
  }

  syncJSONData(data) {
    this.jsonDataSubject.next(data);
  }
}
