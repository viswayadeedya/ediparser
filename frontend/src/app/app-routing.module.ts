import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigureDataComponent } from './home/configure-data/configure-data.component';
import { FileUploadComponent } from './home/file-upload/file-upload.component';

const routes: Routes = [
  { path: 'file-upload', component: FileUploadComponent },
  { path: 'configure-data', component: ConfigureDataComponent },
  { path: '', redirectTo: '/file-upload', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
