package com.example.EDIParser.repository;

import com.example.EDIParser.modal.AccountMappingConfigs;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountConfigRepository extends MongoRepository<AccountMappingConfigs,String> {
    @Query("{account_code: ?0,org_code: ?1}")
    List<AccountMappingConfigs> findByAccountAndOrgCodes(String accountCode, String OrganizationCode);
}
