package com.example.EDIParser.controller;


import com.example.EDIParser.dto.Response;
import com.example.EDIParser.service.EDIReaderService;
import com.example.EDIParser.service.EdiMappingService;
import io.xlate.edi.stream.EDIStreamException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Map;

@RestController
public class EDIReaderController {

    private static final Logger logger = LoggerFactory.getLogger(EDIReaderController.class);

    @Autowired
    EDIReaderService ediReaderService;

    @Autowired
    EdiMappingService ediMappingService;

    @CrossOrigin("*")
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseEntity<Object> uploadEDIFiles(@RequestBody MultipartFile ediFile,@RequestParam(value = "fileType") String fileType) throws IOException, EDIStreamException, XMLStreamException, TransformerException, ParserConfigurationException {

        if(!ediFile.isEmpty()){
            return  ediReaderService.processEdiFile(ediFile,fileType);
        }
        else{
            String errorMessage = "File must be not null or empty";
            logger.info(errorMessage);
            return new ResponseEntity<>(new Response(errorMessage), HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/getEdiMappings", method = RequestMethod.GET)
    public Map<String,String> getEdiMappings(@RequestParam(value = "account_code") String accountCode,
                                             @RequestParam(value = "org_code") String organizationCode) {
        Map<String, String> response = null;
        try {
            if (!accountCode.isEmpty() && !organizationCode.isEmpty()) {
                response = ediMappingService.getMappings(accountCode, organizationCode);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
