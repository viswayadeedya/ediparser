package com.example.EDIParser.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EdiMappingColumns {

    @Field("key")
    private String key;

    @Field("value")
    private String value;
}
