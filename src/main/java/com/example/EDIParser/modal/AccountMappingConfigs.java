package com.example.EDIParser.modal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;
import java.util.Map;

@Document(collection = "account_mapping_configs")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AccountMappingConfigs extends BaseEntity {

    @Field(value ="account_code")
    private String accountCode;

    @Field(value = "org_code")
    private String organizationCode;

    @Field(value = "mapping_columns")
    private List<EdiMappingColumns>mappingColumns;

}
