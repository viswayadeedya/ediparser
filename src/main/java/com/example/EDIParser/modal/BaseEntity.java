package com.example.EDIParser.modal;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;

import java.time.LocalDateTime;
@Data
@EqualsAndHashCode(callSuper = false)
public class BaseEntity {
    @Id
    protected String id;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;
    private LocalDateTime deleted_at;
}
