package com.example.EDIParser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdiParserApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdiParserApplication.class, args);
	}

}
