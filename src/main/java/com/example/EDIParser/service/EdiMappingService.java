package com.example.EDIParser.service;

import com.example.EDIParser.modal.AccountMappingConfigs;
import com.example.EDIParser.modal.EdiMappingColumns;
import com.example.EDIParser.repository.AccountConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EdiMappingService {

    @Autowired
    AccountConfigRepository accountConfigRepository;

    public Map<String, String> getMappings(String accountCode, String organizationCode) {
        List<AccountMappingConfigs> accountMappingConfigs = accountConfigRepository.findByAccountAndOrgCodes(accountCode, organizationCode);
        Map<String, String> mappings = null;
        if (accountMappingConfigs != null && !accountMappingConfigs.isEmpty()) {
            mappings = getEdiMappings(accountMappingConfigs.get(0).getMappingColumns());
            return mappings;
        }else return Map.of("message","mappings are not present please create mappings manually");
    }

    private Map<String, String> getEdiMappings(List<EdiMappingColumns> mappingColumns) {
        Map<String, String> map = new HashMap<>();
        mappingColumns.forEach(integrationConfigMap -> {
            map.put(integrationConfigMap.getKey(), integrationConfigMap.getValue());
        });
        return map;
    }
}
